from flask import Flask, request, jsonify
from flask_cors import CORS, cross_origin
import math

app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

''' ==========================< HELPER >============================== '''

# Dense rank halper method
def denseRankPlus(in1, in2):
    arr1 = in1.split(",")
    arr2 = in2.split(",")

    output = ""

    # ubah input menjadi integer
    try:
        for i in range(len(arr1)):
            arr1[i] = int(arr1[i])

        for i in range(len(arr2)):
            arr2[i] = int(arr2[i])
    except ValueError:
        output = "Value error"
        return output
    except:
        output = "error"
        return output
    print(arr1)
    print(arr2)
    
    arr1 = sorted(arr1+arr2, reverse=True)

    # membuat set untuk menentukan dense rank
    set1 = sorted(list(set(arr1)), reverse=True)

    # mengambil rank input ke dua, dihitung dari set yang dibuat
    rank = []
    for i in arr2:
        if i in set1:
            rank.append(set1.index(i)+1)

    output = {'rank':rank, 'list':arr1}

    return output


def chipper(txt, key, typ):
    geserL = ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"]
    txt = txt.lower()
    key = key.lower()
    result = ""

    # encrypt
    if typ == "e":
        for i in range(len(txt)):
            if(txt[i] != " " and ord(txt[i]) > 96 and ord(txt[i]) < 123):
                result += chr((ord(txt[i]) + geserL.index(key[i]) - 97) % 26 + 97 )
            else:
                result += txt[i]

    # decrypt            
    if typ == "d":
        for i in range(len(txt)):
            if(txt[i] != " " and ord(txt[i]) > 96 and ord(txt[i]) < 123):
                result += chr((ord(txt[i]) - geserL.index(key[i]) - 97) % 26 + 97 )
            else:
                result += txt[i]

    return result

def enkripsi(val, key, typ):
    result = ""

    # make length of key greater then value
    if len(val) > len(key):
        key = key * math.ceil(len(val) / len(key))

    # create matching key
    arrval = list(val)
    n = 0
    for i in range(len(arrval)):
        if arrval[i] != " ":
            arrval[i] = key[n]
            n+=1
    
    genKey = "".join(arrval)

    result = chipper(val, genKey, typ)
    
    return result

''' =========================< CONTROLLER >========================== '''

@app.route('/')
def hello():
    return "Hello World!"

@app.route("/rank", methods=["POST"])
@cross_origin()
def denseRankController():
    output = denseRankPlus(
        request.json['array1'],
        request.json['array2']
    )    
    if "error" not in output:
        return jsonify({'respCode': 200, 'content': output })
    else:
        return jsonify({'respCode': 500, 'content': output })

@app.route("/chipper", methods=["POST"])
@cross_origin()
def chipperController():
    output = enkripsi(
        request.json['value'],
        request.json['key'],
        # type e => encrypt, d => decrypt
        request.json['type']
    )
    return jsonify({'respCode': 200, 'content': output })


if __name__ == '__main__':
    app.run()
