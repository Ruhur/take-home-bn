# Take Home Bn

take home project backend

1. API dense rank
uri: http://<service-name>/rank
method: post
Body: {
	"array1": "90,90,80,70,75,80",
	"array2": "45, 85, 75"
}
*note: body tidak boleh ada string(contoh: a, b, 12, 23)

output: {
    {
    "content": {
        "list": [ 90, 90, 85, 80, 80, 75, 75, 70, 45 ],
        "rank": [ 6, 2, 4 ]
    },
    "respCode": 200
    }
}

value error output: {
    "content": "Value error",
    "respCode": 500
}

2. API enkripsi
uri: http://<service-name>/chipper
method: post
Body: {
	"value":"ke zona hijau",
	"key":"cabik",
	"type":"e"
}
*note: type harus bernilai "e" jika ingin melakukan enkripsi

output: {
    "content": "me awxc hjrkw",
    "respCode": 200
}


3. API dekripsi
uri: http://<service-name>/chipper
method: post
Body: {
	"value":"me awxc hjrkw",
	"key":"cabik",
	"type":"d"
}
*note: type harus bernilai "d" jika ingin melakukan dekripsi

output: {
    "content": "ke zona hijau",
    "respCode": 200
}